﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InteractionManager : MonoBehaviour
{
    public GameObject welcomeBoard;
    public Text usernameTxt;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void activeWelcomeScree(string username)
    {
        usernameTxt.text = username;
        welcomeBoard.SetActive(true);
    }
}
