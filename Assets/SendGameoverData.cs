﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendGameoverData : MonoBehaviour
{
    private void OnEnable()
    {
		GameOver tempGO = GameObject.FindObjectOfType<GameOver>();
		if (tempGO != null)
		{
		tempGO.SetLevelScore(FindObjectOfType<ScoreManager>().GetScore(), 10, FindObjectOfType<ScoreManager>().GetBrokenLinesCounter());
		}
	}
}
