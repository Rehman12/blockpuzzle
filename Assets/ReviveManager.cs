﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ReviveManager : MonoBehaviour
{
    public GameObject OptionalMenu, ReviveButtons;
    public Image cooldown;
    public float waitTime = 15;
    public Sprite AdRevive, FreeRevive;
    public Image MainImage;

    // Update is called once per frame

    private void Start()
    {

        if (StackManager.Instance._isFree)
        {
            MainImage.sprite = FreeRevive;
        }
        else
        {
            MainImage.sprite = AdRevive;
        }


        if (StackManager.Instance.reviveCounter == 0)
        {
            StartCoroutine(startTimer());
        }
        else
        {
         
            OptionalMenu.SetActive(true);
            ReviveButtons.SetActive(false);
        }
    }



    IEnumerator startTimer()
    {



        while (cooldown.fillAmount > 0  && Application.internetReachability != NetworkReachability.NotReachable)
        {
           
            //Reduce fill amount over 30 seconds
            cooldown.fillAmount -= 1.0f / waitTime * Time.deltaTime;
            yield return null;
        }
       
        OptionalMenu.SetActive(true);
        ReviveButtons.SetActive(false);
    }
}
