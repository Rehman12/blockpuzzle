﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{

	[SerializeField] Text txtScore;
	//	[SerializeField] public Text txtBestScore;
	[SerializeField] private Text txtCoinReward;
	public GameObject main, adtext;


	public void SetLevelScore(int score, int coinReward, int BrokenLines)
	{
		/// we have removed the highscore text but we didnt remove it from script, thats why it was empty when we try to assign something to empty variable it return null pointer, that mean left side object is missing or it can be right too
		//	txtBestScore.text = score.ToString();
		if (StackManager.Instance.Ld.UID.Length <= 0) {
			return;
		}




			if (GameController.gameMode.ToString() == "CLASSIC")
			{
			StartCoroutine(StackManager.Instance.Ld.UPG.scoreandLine(StackManager.Instance.Ld.UID, "classic", score, BrokenLines,StackManager.Instance.bombcounterss, StackManager.Instance.line5counter, StackManager.Instance.placingcounter));
			StackManager.Instance.bombcounterss = 0;
			StackManager.Instance.line5counter = 0;
			StackManager.Instance.placingcounter = 0;
		    }
			else if (GameController.gameMode.ToString() == "TIMED")
			{
			StartCoroutine(StackManager.Instance.Ld.UPG.scoreandLine(StackManager.Instance.Ld.UID, "timed", score, BrokenLines, StackManager.Instance.bombcounterss, StackManager.Instance.line5counter, StackManager.Instance.placingcounter));
		    StackManager.Instance.bombcounterss = 0;
			StackManager.Instance.line5counter = 0;
			StackManager.Instance.placingcounter = 0;
		    }

			else if (GameController.gameMode.ToString() == "BLAST")
			{
			StartCoroutine(StackManager.Instance.Ld.UPG.scoreandLine(StackManager.Instance.Ld.UID, "blast", score, BrokenLines, StackManager.Instance.bombcounterss, StackManager.Instance.line5counter, StackManager.Instance.placingcounter));
			StackManager.Instance.bombcounterss = 0;
			StackManager.Instance.line5counter = 0;
			StackManager.Instance.placingcounter = 0;
		    }
			else if (GameController.gameMode.ToString() == "ADVANCE")
			{
			StartCoroutine(StackManager.Instance.Ld.UPG.scoreandLine(StackManager.Instance.Ld.UID, "advance", score, BrokenLines, StackManager.Instance.bombcounterss, StackManager.Instance.line5counter, StackManager.Instance.placingcounter));
			StackManager.Instance.bombcounterss = 0;
			StackManager.Instance.line5counter = 0;
			StackManager.Instance.placingcounter = 0;
		    }
			else if (GameController.gameMode.ToString() == "CHALLENGE")
			{
				StartCoroutine(StackManager.Instance.Ld.UPG.scoreandLine(StackManager.Instance.Ld.UID, "challenge", score, BrokenLines, StackManager.Instance.bombcounterss, StackManager.Instance.line5counter, StackManager.Instance.placingcounter));
				StackManager.Instance.bombcounterss = 0;
			    StackManager.Instance.line5counter = 0;
			    StackManager.Instance.placingcounter = 0;
		    }

		


		


		//txtCoinReward.text = string.Format("{0:#,#.}", coinReward.ToString("0"));

		CurrencyManager.Instance.AddCoinBalance(coinReward);
	}

	public void setTextScore()
    {
		txtScore.text = string.Format("{0:#,#.}", FindObjectOfType<ScoreManager>().GetScore().ToString("0"));
	}


	public void OnHomeButtonPressed()
	{
		if (InputManager.Instance.canInput())
		{
			AudioManager.Instance.PlayButtonClickSound();
			StackManager.Instance.OnCloseButtonPressed();
			StackManager.Instance.reviveCounter = 0;
			GameObject.FindObjectOfType<GamePlay>()._isGameover = false;
		}
	}

	public void OnReplayButtonPressed()
	{
		if (InputManager.Instance.canInput())
		{
			AudioManager.Instance.PlayButtonClickSound();
			StackManager.Instance.RestartGamePlay();
			StackManager.Instance.reviveCounter = 0;
			GameObject.FindObjectOfType<GamePlay>()._isGameover = false;
		}
	}

	public void Rate()
	{
		Application.OpenURL("https://codecanyon.net/user/bimbimnet/portfolio");
	}


	public void adbutton()
	{
		if (StackManager.Instance.reviveCounter == 1)
			return;


		main.SetActive(false);
		adtext.SetActive(true);
		if (!StackManager.Instance._isFree)
		{
			GameObject.FindObjectOfType<AdsControl>().callforad();

		}
        else
        {
			GameObject.FindObjectOfType<UnityPostGet>().FreeAds(StackManager.Instance.Ld.UID);
			GameObject.FindObjectOfType<AdsControl>().giveReward();
			adtext.SetActive(false);
		}
    
		
		//StackManager.Instance.FS.EnterCounter(StackManager.Instance.Ld.Uidusername, temp.ToString());
		StackManager.Instance.reviveCounter = 1;
		GameObject.FindObjectOfType<GamePlay>()._isGameover = false;
		
	}
}
