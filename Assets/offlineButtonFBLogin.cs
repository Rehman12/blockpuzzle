﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class offlineButtonFBLogin : MonoBehaviour
{
    LocalDatabase ld;
    public GameObject Leaderboard;
    // Start is called before the first frame update
    void Start()
    {
        ld = GameObject.FindObjectOfType<LocalDatabase>();

      

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            this.gameObject.SetActive(false);
            Leaderboard.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (ld.UID.Length > 1)
        {
            this.gameObject.SetActive(false);
            Leaderboard.SetActive(true);
        }
        else
        {

            GetComponent<Button>().onClick.AddListener(ButtonLis);
        }

    }


    public void ButtonLis()
    {
        StackManager.Instance.fbManager.FBlogin();
    }
}
