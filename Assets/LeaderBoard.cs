﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoard : MonoBehaviour
{
    public RectTransform parentComp;
    public Transform prefab;
    public GameObject Loading;
    public List<string> getscore = new List<string>();
    public List<string> localUser = new List<string>();
    public Text currentScore;
    public Toggle classic, timed, blast, advance, challenge;
    void Start()
    {

        loadData("classic");
     
    }

   
    public void loadData(string BoardName)
    {
        Loading.SetActive(true);
        if (classic.isOn)
        { 
            StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetHighscore("classic"));
          //  StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetCurrentUserHighscore("classic",StackManager.Instance.Ld.UID));
        }
        else if ( timed.isOn)
        {
         
            StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetHighscore("timed"));
          //  StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetCurrentUserHighscore("timed", StackManager.Instance.Ld.UID));
        }
        else if ( blast.isOn)
        {
          
            StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetHighscore("blast"));
          //  StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetCurrentUserHighscore("blast", StackManager.Instance.Ld.UID));
        }
        else if ( advance.isOn)
        {
           
            StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetHighscore("advance"));
          //  StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetCurrentUserHighscore("advance", StackManager.Instance.Ld.UID));
        }
        else if (challenge.isOn)
        {
         
            StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetHighscore("challenge"));
         //   StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().GetCurrentUserHighscore("challenge", StackManager.Instance.Ld.UID));
        }
       
       
    }


    public IEnumerator CloneText()
    {
      
       
        while (getscore.Count == 0)
        {

         
            yield return null;
        }
        int temp = 1;
        removeAllChild();

        for (int i = 0; i < getscore.Count; i++)
        {
           
            RectTransform clone = Instantiate((RectTransform)prefab);
          

            clone.SetParent(parentComp);
          
            clone.GetComponent<Text>().text = getscore[i];
         
            clone.gameObject.SetActive(true);
          
           
            clone.transform.localScale = Vector3.one;
         
            temp += 1;
        }
        //RectTransform cloneHeader = Instantiate((RectTransform)prefab);
        //cloneHeader.SetParent(parentComp);
        //cloneHeader.GetComponent<Text>().text = "                       ***Your Score***";
        //cloneHeader.gameObject.SetActive(true);
        //cloneHeader.transform.localScale = Vector3.one;

        //RectTransform cloneScore = Instantiate((RectTransform)prefab);
        //cloneScore.SetParent(parentComp);
        //cloneScore.GetComponent<Text>().text = localUser[0].Split('=')[0]+"     "+localUser[0].Split('=')[1]+"     "+localUser[0].Split('=')[2];
        //cloneScore.gameObject.SetActive(true);
        //cloneScore.transform.localScale = Vector3.one;

       Loading.SetActive(false);
        getscore = new List<string>(0);
        localUser = new List<string>(0);
       
    }


    void removeAllChild()
    {
        foreach(RectTransform childss in parentComp)
        {
            if (childss.gameObject.active)
            {
                Destroy(childss.gameObject);
            }
        }
    
    }


    public void backBt()
    {
        AudioManager.Instance.PlayButtonClickSound();
        Destroy(this.gameObject);
    }

    public void ButtonSound()
    {
        AudioManager.Instance.PlayButtonClickSound();
    }
   
}
