﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;


public class Unityads : MonoBehaviour, IUnityAdsListener
{
    private AdsControl AC;

#if UNITY_IOS
    private string gameId = "3964880";
#elif UNITY_ANDROID
    private string gameId = "3964881";
#endif
    public string myPlacementId = "rewardedVideo";

   
    private void Start()
    {
        AC = GameObject.FindObjectOfType<AdsControl>();
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, true);
        callRewardAds();
    }
  

   public void callRewardAds()
    {
        
        ShowRewardedVideo();
    }
    void ShowRewardedVideo()
    {
        Advertisement.Show(myPlacementId);
    }
    public void OnUnityAdsReady(string placementId)
    {
        
        if (placementId == myPlacementId)
        {
           
           
           
        }
    }
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    { 
        if (showResult == ShowResult.Finished)
        {
            AC.giveReward();
        }
        else if (showResult == ShowResult.Skipped)
        {
            AC.failedTime();
        }
        else if (showResult == ShowResult.Failed)
        {
            AC.failedTime();
        }
    }
    public void OnUnityAdsDidError(string message)
    {
     //   AC.failedTime();
    }

    public void OnUnityAdsDidStart(string placementId)
    {
      
    }
}
