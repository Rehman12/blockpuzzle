﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class QuestManager : MonoBehaviour
{
    public RectTransform parentComp;
    public Transform prefab;
    public List<string> getscore = new List<string>();
    public GameObject btnPanel;
    public Image FadeScreen;
    private void OnEnable()
    {
        FadeScreen.enabled = true;
        StartCoroutine(StackManager.Instance.Ld.UPG.StatusBrokenLinesQuest(StackManager.Instance.Ld.UID));
      
    }

  public void CollectingAllbtn()
    {
        FadeScreen.enabled = true;
        AudioManager.Instance.PlayButtonClickSound();
        StartCoroutine(StackManager.Instance.Ld.UPG.CompleteAllbtn(StackManager.Instance.Ld.UID));
        Invoke("refreshServer",1);
    }

    public void backBt()
    {
        AudioManager.Instance.PlayButtonClickSound();
        Destroy(this.gameObject);
    }

    public void refreshServer()
    {
        StartCoroutine(StackManager.Instance.Ld.UPG.StatusBrokenLinesQuest(StackManager.Instance.Ld.UID));
        
    }

    public IEnumerator CloneText()
    {


        while (getscore.Count == 0)
        {


            yield return null;
        }
        int temp = 1;
        removeAllChild();

        for (int i = 0; i < getscore.Count; i++)
        {

            RectTransform clone = Instantiate((RectTransform)prefab);


            clone.SetParent(parentComp);

            clone.GetComponent<Text>().text = getscore[i];

            clone.gameObject.SetActive(true);


            clone.transform.localScale = Vector3.one;

            temp += 1;
        }

        FadeScreen.enabled = false;
        getscore = new List<string>(0);
    }

    void removeAllChild()
    {
        foreach (RectTransform childss in parentComp)
        {
            if (childss.gameObject.active)
            {
                Destroy(childss.gameObject);
            }
        }

    }
}
