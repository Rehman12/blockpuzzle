﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : Singleton<ScoreManager>
{
	public Text txtScore, highScore;
	[SerializeField] private GameObject scoreAnimator;
	[SerializeField] private Text txtAnimatedText;
	private int Score = 0;
	[SerializeField]
	private int BrokenLines = 0;
	List<string> localUser = new List<string>();
	List<string> alluser = new List<string>();
	void Start()
	{
		txtScore.text = Score.ToString();
		localUser = new List<string>(0);
		
		giveHigh();
		StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().Adinfo(StackManager.Instance.Ld.UID));

	}


	public void giveHigh()
	{
		if (GameController.gameMode.ToString() == "CLASSIC")
		{
			StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().SendGR("classic", StackManager.Instance.Ld.UID));

		}
		else if (GameController.gameMode.ToString() == "TIMED")
		{
			StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().SendGR("timed", StackManager.Instance.Ld.UID));
		}

		else if (GameController.gameMode.ToString() == "BLAST")
		{
			StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().SendGR("blast", StackManager.Instance.Ld.UID));
		}
		else if (GameController.gameMode.ToString() == "ADVANCE")
		{
			StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().SendGR("advance", StackManager.Instance.Ld.UID));
		}
		else if (GameController.gameMode.ToString() == "CHALLENGE")
		{
			StartCoroutine(GameObject.FindObjectOfType<UnityPostGet>().SendGR("challenge", StackManager.Instance.Ld.UID));
		}


	}

	public void BrokenLinesCounter(int lineToAdd)
	{
		BrokenLines += lineToAdd;

	}
	public int GetBrokenLinesCounter()
	{
		return BrokenLines;
	}


	public void AddScore(int scoreToAdd, bool doAnimate = true)
	{
		int oldScore = Score;
		Score += scoreToAdd;
		int temphigh = int.Parse(highScore.text);
		if (Score > temphigh)
		{
			highScore.text = Score.ToString();
		}

		StartCoroutine(SetScore(oldScore, Score));

		if (doAnimate)
		{
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePos.z = 0;
			scoreAnimator.transform.position = mousePos;
			txtAnimatedText.text = "+" + scoreToAdd.ToString();
			scoreAnimator.SetActive(true);
		}
	}

	public int GetScore()
	{
		return Score;
	}

	IEnumerator SetScore(int lastScore, int currentScore)
	{
		int IterationSize = (currentScore - lastScore) / 10;

		for (int index = 1; index < 10; index++)
		{
			lastScore += IterationSize;
			txtScore.text = string.Format("{0:#,#.}", lastScore);
			yield return new WaitForEndOfFrame();
		}
		txtScore.text = string.Format("{0:#,#.}", currentScore);
		yield return new WaitForSeconds(1F);
		scoreAnimator.SetActive(false);
	}
}
