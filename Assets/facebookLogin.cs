﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
public class facebookLogin : MonoBehaviour
{
    public bool _iswatching = true;
   
    public InteractionManager IM;
    public LocalDatabase LD;
    public List<string> Leaderboard1 = new List<string>();
    public List<string> Leaderboard2 = new List<string>();
    public List<string> Leaderboard3 = new List<string>();
    public List<string> Leaderboard4 = new List<string>();
    public List<string> Leaderboard5 = new List<string>();
    public List<string> alluser = new List<string>();
    public List<string> Adsuser = new List<string>();
    public string username;
   
    // Use this for initialization
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }


    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
            if (PlayerPrefs.GetInt("userEnter", 0) == 1)
            {
                FBlogin();

                StackManager.Instance.GiveSoundBt();
            }
            //  FBlogin();
            // StartCoroutine(saveData());

        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
    public void FBlogin()
    {

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (StackManager.Instance.firstScreen != null)
            {
                StackManager.Instance.firstScreen.SetActive(false);
            }
            StackManager.Instance.fbManager.GetComponent<WboardManager>().Guestwelcome.gameObject.SetActive(false
                );
            StackManager.Instance.welcomeUsernameTxt.gameObject.SetActive(true);
            FB.LogInWithReadPermissions(
            new List<string>() { "public_profile", "email" },
             AuthCallback
             );
        }
    }
    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {

            FB.API("/me?fields=id,first_name,last_name,email", HttpMethod.GET, graphCallback);
          

        }
        else
        {

            PlayerPrefs.DeleteKey("UID");
            PlayerPrefs.DeleteKey("username");
            PlayerPrefs.DeleteKey("email");
            PlayerPrefs.DeleteKey("userEnter");
            StackManager.Instance.Ld.UID = "";
            StackManager.Instance.Ld.Username = "";
            StackManager.Instance.Ld.Email = "";


            if (StackManager.Instance.firstScreen != null)
            {
                StackManager.Instance.firstScreen.SetActive(true);
               
            }
            StackManager.Instance.welcomeUsernameTxt.gameObject.SetActive(false);
            //    Debug.Log("User cancelled login");
        }
    }


   

    public void callLeader_1(string UID, string Email, string Username)
    {
        if (PlayerPrefs.GetInt("userEnter", 0) == 0)
        {
            LD.Uidusername = LD.UID + " = " + LD.Email.Split('@')[0] + "at" + LD.Email.Split('@')[1].Split('.')[0] + LD.Email.Split('@')[1].Split('.')[1];


          
            StartCoroutine(GettingScore());

         //   StackManager.Instance.FS.getCounter(LD.Uidusername);
            PlayerPrefs.SetInt("userEnter", 1);
        }

       


    }

    public IEnumerator GettingScore()
    {
        float a = 100;
        while (Leaderboard1.Count == 0 && a > 0)
        {

            print("waiting");
            a -= 1f;
            yield return null;
        }

        int temp = 0;
        for(int i = 0; i < Leaderboard1.Count; i++)
        {

            temp = 1;
        }


      
    }
    private void graphCallback(IGraphResult result)
    {
        Debug.Log("hi= " + result.RawResult);
     
        string firstname;
        string lastname;
        string id;
        string email;
       
        if (result.ResultDictionary.TryGetValue("first_name", out firstname))
        {
            Debug.Log(firstname.ToString());
        }
        if (result.ResultDictionary.TryGetValue("last_name", out lastname))
        {

        }
        if (result.ResultDictionary.TryGetValue("id", out id))
        {
         
        }
        if (result.ResultDictionary.TryGetValue("email", out email))
        {
            
        }
        IM.activeWelcomeScree(firstname + lastname) ;
        LD.updateDate(id,email, firstname + lastname);
        callLeader_1(id, email, firstname + lastname);
        StartCoroutine(StackManager.Instance.Ld.UPG.LoginUser(id,email,firstname,lastname));
      
    }
}
