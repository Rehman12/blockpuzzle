﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BombCounter : MonoBehaviour
{
    public Text counter;
    int tempInt;
    public Image hint;
    private void Update()
    {
        hint.enabled = false;
        if (!counter.gameObject.activeSelf)
        {
            return;
        }

        //else if (int.Parse(counter.text) == 0)
        //{
        //    tempInt = 0;
        //    hint.enabled = false;
        //    GetComponent<Image>().sprite = null;
        //    GetComponent<Image>().color = new Color(0, 0, 0, 0);
        //    counter.gameObject.SetActive(false);
        //}

        tempInt = int.Parse(counter.text);
        if(tempInt < 4)
        {
            hint.enabled = true;
        }
        else
        {
            hint.enabled = false;
        }


       
    }

    public void closeBomb()
    {
        this.transform.parent.GetComponent<Block>().ClearBlock(4);
    }
}
