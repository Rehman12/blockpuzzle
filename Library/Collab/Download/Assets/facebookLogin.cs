﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
public class facebookLogin : MonoBehaviour
{
    public bool _iswatching = true;
    public Firebase_Script FS;
    public InteractionManager IM;
    public LocalDatabase LD;
    Firebase.Auth.FirebaseUser newUser;
    // Use this for initialization
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }


    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
           
                FBlogin();
                StartCoroutine(saveData());
            
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
    public void FBlogin()
    {
        List<string> permissions = new List<string>();
        permissions.Add("public_profile");
        permissions.Add("email");
        FB.LogInWithReadPermissions(permissions, AuthCallback);
    }
    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            Firebase.Auth.FirebaseAuth auth =
            Firebase.Auth.FirebaseAuth.DefaultInstance;
            List<string> permissions = new List<string>();

            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.TokenString);
            Debug.Log(aToken.UserId);


            Firebase.Auth.Credential credential =

            Firebase.Auth.FacebookAuthProvider.GetCredential(aToken.TokenString);
            auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                   // Debug.LogError("SignInWithCredentialAsync was canceled.");
                    OnHomeButtonPressed();
                    return;
                }
                if (task.IsFaulted)
                {
                 //   Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                    OnHomeButtonPressed();
                    return;
                }

                 newUser = task.Result;
           //     Debug.LogFormat("User signed in successfully: {0} ({1})",
             //           newUser.DisplayName, newUser.UserId);
                _iswatching = false;
             //   GetComponent<FirebaseDatabase>().writeNewUser(newUser.UserId,newUser.DisplayName,newUser.Email);
            });
            // Print current access token's granted permissions
            //foreach(string perms in aToken.Permissions)
            //{
            // Debug.Log(perms);
            //}
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }


    public IEnumerator saveData()
    {
        while (_iswatching)
        {
            yield return null;
        }
   
      

        // LocalDatabase LD = GameObject.FindObjectOfType<LocalDatabase>();

        callLeader_1();
        IM.activeWelcomeScree(newUser.DisplayName);
        LD.updateDate(newUser.UserId, newUser.Email, newUser.DisplayName);


    }


    public void callLeader_1()
    {
        print(PlayerPrefs.GetString("UID", "").Length);
        
            FS.Firebase_LeaderBoard_1(LD.UID, LD.Email, LD.Username, "0");
            FS.Firebase_LeaderBoard_2(LD.UID, LD.Email, LD.Username, "0");
            FS.Firebase_LeaderBoard_3(LD.UID, LD.Email, LD.Username, "0");
            FS.Firebase_LeaderBoard_4(LD.UID, LD.Email, LD.Username, "0");
            FS.Firebase_LeaderBoard_5(LD.UID, LD.Email, LD.Username, "0");
        

    }

    public void OnHomeButtonPressed()
    {

       
        AudioManager.Instance.PlayButtonClickSound();
        StackManager.Instance.OnCloseButtonPressed();
        StackManager.Instance.CloseGameplay();

    }
}