﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class UnityPostGet : MonoBehaviour
{
    // public InputField PostInput;
    // public InputField GetInput;




    //public void SendPostRequest()
    //{
    //  //  string Message = PostInput.text;
    //    StartCoroutine(SendPR(Message));
    //}

    //public void SendGetRequest()
    //{
    //  //  string Message = PostInput.text;
    //    StartCoroutine(SendGR(Message));
    //}

    // this one is for adclick


    public IEnumerator LoginUser(string fbuid, string useremail, string firstname, string lastname)
    {
        WWWForm form = new WWWForm();
        form.AddField("FBUID", fbuid);
        form.AddField("useremail", useremail);
        form.AddField("firstname", firstname);
        form.AddField("lastname", lastname);
        ;


        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/onlogin.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
                Debug.Log("Response Text from the server = " + responseText);
            }
        }
    }



    public IEnumerator SendPR(Dictionary<string, string> Infos)
    {
        WWWForm form = new WWWForm();
        form.AddField("userxp", "1");
        form.AddField("facepuzzle", "1");
        form.AddField("FBUID", Infos["FBUID"]);


        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/onrevive.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
                Debug.Log("Response Text from the server = " + responseText);
            }
        }
    }


    public IEnumerator Adinfo(string UID)
    {
        WWWForm form = new WWWForm();
        form.AddField("FBUID", UID);


        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/video_ads_available.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
                Debug.Log("Response Text from the server = " + responseText);
                StackManager.Instance.adcounter = int.Parse(responseText);
                StackManager.Instance.manageAds();
            }
        }
    }
    public IEnumerator FreeAds(string UID)
    {
        WWWForm form = new WWWForm();
        form.AddField("FBUID", UID);


        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/onfreerevive.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
           
        }
    }
   
    public IEnumerator StatusBrokenLinesQuest( string UID)
    {
        WWWForm form = new WWWForm();
        form.AddField("FBUID", UID);
        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/usertasks.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
              //  print(responseText);
                QuestManager QM = FindObjectOfType<QuestManager>();


                if (QM != null)
                {

                    string[] splitArray = responseText.Split(char.Parse(","));
                    int tempcounter = 0;
                    string tempvalue = "";
                    for (int i = 0; i < splitArray.Length - 1; i++)
                    {
                        if(splitArray[i].ToLower() == "done")
                        {
                            QM.btnPanel.SetActive(true);
                         
                        }
                        tempcounter += 1;

                        tempvalue += "    " + splitArray[i] + "            ";
                        if (tempcounter > 1)
                        {
                            tempcounter = 0;
                            QM.getscore.Add(tempvalue);

                            tempvalue = "";
                        }
                    }
                }
                StartCoroutine(QM.CloneText());
            }
        }
    }


    public IEnumerator scoreandLine(string UID,string MethodName, int score,int brokenCounter, int bombcounter, int Line5counter, int blockplacecounter)
    {
        WWWForm form = new WWWForm();
       
        form.AddField("FBUID", UID);
        form.AddField(MethodName, score.ToString());
        form.AddField("brokenlines", brokenCounter.ToString());
        form.AddField("bombscleared",bombcounter);
        form.AddField("broken3lines", Line5counter);
        form.AddField("blocksplaced", blockplacecounter);
        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/ongameover.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
              
            }
        }
    }

    public IEnumerator CompleteAllbtn(string UID)
    {
        WWWForm form = new WWWForm();
        form.AddField("FBUID", UID);
        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/collectrewards.php", form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
              
            }
        }
    }


    // this one is for send score
    //public IEnumerator SendScore(string MethodName, int score, string UID)
    //{
    //    WWWForm form = new WWWForm();
    //    form.AddField(MethodName, score.ToString());
    //    form.AddField("FBUID", UID);
    //    using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/sendscore.php", form))
    //    {
    //        www.downloadHandler = new DownloadHandlerBuffer();
    //        yield return www.SendWebRequest();

    //        if (www.isNetworkError)
    //        {
    //            Debug.Log(www.error);
    //        }
    //        else
    //        {
    //            string responseText = www.downloadHandler.text;
    //            Debug.Log("Response Text from the server = " + responseText);
    //        }
    //    }
    //}
    // this one is for getting highscore
    public IEnumerator SendGR(string gamemode, string UID)
    {

        WWWForm form = new WWWForm();
        form.AddField("FBUID", UID);
        form.AddField("gamemode", gamemode);

        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/gethighscore.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
                Debug.Log("Response Text from the server = " + responseText);
                GameOver tempGO = GameObject.FindObjectOfType<GameOver>();
                ScoreManager tempSM = GameObject.FindObjectOfType<ScoreManager>();
                //if(tempGO != null)
                //{
                //    tempGO.txtBestScore.text = responseText;
                //}
                if (tempSM != null && tempGO == null)
                {
                    tempSM.highScore.text = responseText;
                }

            }
        }
    }
    //   List one is for getting leaderboard score
    public IEnumerator GetHighscore(string gamemode)
    {

        WWWForm form = new WWWForm();

        form.AddField("gamemode", gamemode);

        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/leaderboarddata.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
                Debug.Log("Response Text from the server = " + responseText);
                string[] splitArray = responseText.Split(char.Parse(","));

                LeaderBoard LB = GameObject.FindObjectOfType<LeaderBoard>();


               
                int tempcounter = 0;
                string tempvalue = "";
                for (int i = 0; i < splitArray.Length - 1; i++)
                {
                    tempcounter += 1;
                    tempvalue += "    " + splitArray[i] + "            ";
                    if (tempcounter > 2)
                    {
                        tempcounter = 0;

                        LB.getscore.Add(tempvalue);
                        tempvalue = "";
                    }
                }

               

                StartCoroutine(GetCurrentUserRankLeaderboard(gamemode, StackManager.Instance.Ld.UID));

                StartCoroutine(LB.CloneText());

            }
        }
    }
    // this one is for leaderboard too
    public IEnumerator GetCurrentUserRankLeaderboard(string gamemode, string UID)
    {
        print("Highscore");
        WWWForm form = new WWWForm();
        form.AddField("FBUID", UID);
        form.AddField("gamemode", gamemode);

        using (UnityWebRequest www = UnityWebRequest.Post("http://gamerswallet.com/links/facepuzzle/userrank.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;
                Debug.Log("Response Text from the server = " + responseText);
                string[] splitArray = responseText.Split(char.Parse(","));

                LeaderBoard LB = GameObject.FindObjectOfType<LeaderBoard>();

                int tempcounter = 0;
                string tempvalue = "";
                for (int i = 0; i < splitArray.Length - 1; i++)
                {
                    tempcounter += 1;
                    tempvalue += "        " + splitArray[i] + "          ";
                    if (tempcounter > 2)
                    {
                        tempcounter = 0;
                        if (LB != null)
                        {
                            LB.currentScore.text = tempvalue;
                        }

                        tempvalue = "";
                    }
                }

            }
        }
    }

}
