﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SocialPlatforms;
using GoogleMobileAds.Api;
using System;
using ChartboostSDK;

public class AdsControl : MonoBehaviour
{

    public GoogleMobileAdsBanner adMob;
    public ChartboostExample chartBoost;
    public Unityads unityAds;
    [SerializeField]private int adsCounter;

    private void Start()
    {
        adsCounter = PlayerPrefs.GetInt("adcounter",0);
    }


    public void callforad()
    {

        //unityAds.callRewardAds();
        //adsCounter = 0;
        //PlayerPrefs.SetInt("adcounter", adsCounter);
        // return

        if (adsCounter == 0)
        {

            adMob.RewardVideos();
            adsCounter += 1;
            PlayerPrefs.SetInt("adcounter", adsCounter);
            return;

        }
        else if (adsCounter == 1)
        {

            chartBoost.showRewardAds();
            adsCounter += 1;
            PlayerPrefs.SetInt("adcounter", adsCounter);
            return;


        }
        else if (adsCounter == 2)
        {
            unityAds.callRewardAds();
            adsCounter = 0;
            PlayerPrefs.SetInt("adcounter", adsCounter);
            return;
        }





    }
    public void giveReward()
    {
       
        GamePlay temp = GameObject.FindObjectOfType<GamePlay>();
        if(temp != null)
        {
           
                LocalDatabase LD = GameObject.FindObjectOfType<LocalDatabase>();
            if (StackManager.Instance.adcounter > 0)
            {
                StartCoroutine(LD.UPG.SendPR(LD.tempInfo));
            }

            StackManager.Instance.CloseRescue();

            temp.ExecuteRescue();

            GameObject tempWaitScreen = GameObject.Find("WaitingTime");
            if (tempWaitScreen != null)
            {
                tempWaitScreen.SetActive(false);
            }
        }
       
    }

    public void failedTime()
    {
        Time.timeScale = 1;
        GameOver temp = GameObject.FindObjectOfType<GameOver>();
        if (temp != null)
        {
            temp.adtext.SetActive(false);
            temp.main.SetActive(true);

            ReviveManager RM = GameObject.FindObjectOfType<ReviveManager>();
            RM.OptionalMenu.SetActive(true);
            RM.ReviveButtons.SetActive(false);
        }
    }
}

